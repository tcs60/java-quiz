package quiz1;

import java.util.Scanner;

public class CountChars {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Please write a sentence. End with ENTER:");
        String sntc = scan.nextLine();
        int count=0;
        for (int i = 0; i < sntc.length(); i++) {
            count++;
        }
        System.out.println("Total of characters in the sentence: "+count);
    }
}
