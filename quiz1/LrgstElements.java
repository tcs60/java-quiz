package quiz1;

import java.util.Scanner;

public class LrgstElements {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Define the lenght of the array:");
        int fst=0,snd=0,trd=0, arr_len=scan.nextInt();
        int[] array = new int[arr_len];
        System.out.println("Please enter the numbers:");
        for (int i = 0; i < arr_len; i++) {
            array[i]=scan.nextInt();
        }
        for (int i = 0; i < array.length; i++) {
            if (array[i]>fst){
                trd = snd;
                snd = fst;
                fst = array[i];
            }
            else if (array[i]>snd){
                trd = snd;
                snd = array[i];
            }
            else if (array[i]>trd){
                trd = array[i];
            }
        }
        System.out.println("Original Array:");
        System.out.print("[ ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]+" ");
        }
        System.out.println("]\n3 largest elements of the array:\n"+fst+" "+snd+" "+trd);
    }
}
