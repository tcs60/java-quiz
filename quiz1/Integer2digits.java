package quiz1;

import java.util.Scanner;

public class Integer2digits {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter integer to break:");
        int num = scan.nextInt();
        String number = String.valueOf(num);
        char[] digits = number.toCharArray();
        for (int i = 0; i < digits.length; i++) {
            System.out.print("[ "+digits[i]+" ] ");
        }
    }
}
