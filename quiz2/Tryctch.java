/*  Try catch in java is used to have counter measures if something goes wrong in compilation
* and execution of a code
* Try sentence 'tries' to do certain action
* if it fails to do it, catch sentence is applied
*
* */
package quiz2;

public class Tryctch {
    public static void main(String[ ] args) {
        try {
            int[] myNumbers = {1, 2, 3}; //This array only has 3 items
            System.out.println(myNumbers[10]); //This code wants to print the 11th item of the array, it doesnt exist
        } catch (Exception e) { //If something goes wrong, this code will be executed
            System.out.println("Something went wrong."); //Indeed, something went wrong
        }
    }
}
