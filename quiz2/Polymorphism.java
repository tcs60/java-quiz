/* How does Java implement polymorphism, give code example:
 * Java implements polymorphism by doing the same action (in this case, drawing)
 * but getting different results.
 * This is done by defining the action in a class and creating an object of that class.
 * Java knows this object is overwritten everytime you call a new class and giving it the "same" object.
 */
package quiz2;

class Shape{
     void draw(){
         System.out.println("drawing...");
     }
}
class Triangle extends Shape{
    void draw(){
        System.out.println("Drawing a triangle");
    }
}
class Square extends Shape{
    void draw(){
        System.out.println("Drawing a square");
    }
}
class Star extends Shape{
    void draw(){
        System.out.println("Drawing a star");
    }
}
public class Polymorphism {
    public static void main(String[] args) {
        Shape s;
        s=new Triangle();
        s.draw();
        s=new Square();
        s.draw();
        s=new Star();
        s.draw();
    }
}