/* How does Java implement Inheritance, give code example:
 * Java looks for a superclass (parent) and subclasses (offspring).
 * Every class in java is potentially a superclass.
 * Subclasses have by default all the atributes of its parent class, thats inheritance.
 */
package quiz2;

class Parent{
    String hair = "brown";
    String eyes = "green";
}
class Offspring extends Parent{
    String eyes = "blue";
}

public class Inheritance {
    public static void main(String args[]){
        Offspring o=new Offspring();
        System.out.println("Offspring has hair color: "+o.hair);
        System.out.println("Offspring has eyes color: "+o.eyes);
    }
}